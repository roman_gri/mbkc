package SecurityLevelSetter;

import Controllers.FilesHandler;
import FileSystemWork.DirectoryReader;
import FileSystemWork.WindowsSymlinksCreator;
import Security.SecurityLevels;
import models.Role;
import models.User;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

/**
 * Roman 06.04.2016.
 */
public class SecurityLevelSetting extends JFrame {
    private JList dirsList;
    private JPanel rootPanel;
    private JTextField securityLevelTextField;
    private JButton setButton;
    private JButton addDirButton;
    private JTabbedPane tabbedPane1;
    private JList usersList;
    private JList userRolesList;
    private JButton addUserButton;
    private JTextField newUserTextField;
    private JComboBox chooseRoleComboBox;
    private JButton addUserRoleButton;
    private JList rolesList;
    private JCheckBox secretCheckBox;
    private JCheckBox topSecretCheckBox;
    private JButton addRoleButton;
    private JTextField newRoleTextField;
    private JList admRolesUsersList;
    private JList admRolesRolesList;
    private JCheckBox canAssignCheckBox;
    private JCheckBox canRevokeCheckBox;
    private JButton deleteUserRoleButton;
    private JComboBox<User> currentUserComboBox;
    private JLabel infoLabel;
    private FilesHandler filesHandler = new FilesHandler();
    private Set<User> users;
    private Set<Role> roles;

    @SuppressWarnings("unchecked")
    private SecurityLevelSetting() {
        //filesHandler.setCurrentDirPath("/");
        filesHandler.setCurrentDirPath(System.getenv("ROOT_DIR"));
        filesHandler.setDirContent(DirectoryReader.getDirsList(filesHandler.getCurrentDirPath()));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(rootPanel);
        pack();

        if (Files.exists(Paths.get("users.dat"))) {
            try (FileInputStream fis = new FileInputStream("users.dat");
                 ObjectInputStream oin = new ObjectInputStream(fis)) {
                users = (Set<User>) oin.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            users = new HashSet<>();
        }

        refreshUsersList();

        currentUserComboBox.setSelectedIndex(0);

        if (Files.exists(Paths.get("roles.dat"))) {
            try (FileInputStream fis = new FileInputStream("roles.dat");
                 ObjectInputStream oin = new ObjectInputStream(fis)) {
                roles = (Set<Role>) oin.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            roles = new HashSet<>();
        }

        refreshRolesLists();

        setVisible(true);
        usersList.addListSelectionListener(e -> {
            refreshUsersRolesList();
        });
        dirsList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 2) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (filesHandler.getCurrentDirPath().getParent() != null) {
                        if (index == 0) {
                            filesHandler.goToParentDir();
                        } else {
                            filesHandler.goToDir(index - 1);
                        }
                    } else {
                        filesHandler.goToDir(index);
                    }
                    refreshDirsList();
                    securityLevelTextField.setText(SecurityLevels.int2String(filesHandler.getCurrentDirSecurityLevel()));
                }
            }
        });
        admRolesUsersList.addListSelectionListener(e -> {
            if (admRolesRolesList.getSelectedValue() != null) {
                User user = (User) admRolesUsersList.getSelectedValue();
                Role role = (Role) admRolesRolesList.getSelectedValue();
                canAssignCheckBox.setSelected(user.canAssign(role));
                canRevokeCheckBox.setSelected(user.canRevoke(role));
            }
        });
        admRolesRolesList.addListSelectionListener(e -> {
            if (admRolesUsersList.getSelectedValue() != null) {
                User user = (User) admRolesUsersList.getSelectedValue();
                Role role = (Role) admRolesRolesList.getSelectedValue();
                canAssignCheckBox.setSelected(user.canAssign(role));
                canRevokeCheckBox.setSelected(user.canRevoke(role));
            }
        });
        setButton.addActionListener(e -> {
            DirectoryReader.setSecurityLevelRecursively(filesHandler.getCurrentDirPath(),
                    SecurityLevels.string2Int(securityLevelTextField.getText()));
        });
        addDirButton.addActionListener(e -> {
            System.out.println(System.getenv("ROOT_DIR"));
            WindowsSymlinksCreator.createLink(Paths.get(System.getenv("ROOT_DIR")).resolve(filesHandler.getCurrentDirPath().getFileName()),
                    filesHandler.getCurrentDirPath());
        });
        addUserButton.addActionListener(e -> {
            if (newUserTextField.getText().equals("")) return;

            if (!users.add(new User(newUserTextField.getText()))) {
                System.err.println("User already exists");
            } else {
                refreshUsersList();
                saveUsersAndRoles();
            }
        });
        addRoleButton.addActionListener(e -> {
            if (newRoleTextField.getText().equals("")) return;
            if (!roles.add(new Role(newRoleTextField.getText(), secretCheckBox.isSelected(), topSecretCheckBox.isSelected()))) {
                System.err.println("Role already exists");
            } else {
                refreshRolesLists();
                saveUsersAndRoles();
            }
        });
        addUserRoleButton.addActionListener(e -> {
            Role roleToAdd = (Role) chooseRoleComboBox.getSelectedItem();
            User user = (User) usersList.getSelectedValue();
            if (user == null || roleToAdd == null) return;

            User currentUser = (User) currentUserComboBox.getSelectedItem();
            if (!currentUser.canAssign(roleToAdd)) {
                infoLabel.setText(currentUser + " doesn't have right to add " + roleToAdd);
                return;
            }

            if (user.addRole(roleToAdd)) {
                infoLabel.setText(currentUser + " add role " + roleToAdd + " to " + user);
                refreshUsersRolesList();
                saveUsersAndRoles();
            } else {
                System.err.println("Role already exists");
            }
        });

        deleteUserRoleButton.addActionListener(e -> {
            Role roleToDelete = (Role) userRolesList.getSelectedValue();
            User user = (User) usersList.getSelectedValue();
            if (user == null || roleToDelete == null) return;

            User currentUser = (User) currentUserComboBox.getSelectedItem();
            if (!currentUser.canRevoke(roleToDelete)) {
                infoLabel.setText(currentUser + " doesn't have right to delete " + roleToDelete);
                return;
            }

            if (user.deleteRole(roleToDelete)) {
                infoLabel.setText(currentUser + " delete role " + roleToDelete + " from " + user);
                refreshUsersRolesList();
                saveUsersAndRoles();
            } else {
                System.err.println("User doesn't have this role");
            }
        });

        topSecretCheckBox.addChangeListener(e -> {
            if (topSecretCheckBox.isSelected()) secretCheckBox.setSelected(true);
        });
        canAssignCheckBox.addChangeListener(e -> {
            if (admRolesRolesList.getSelectedValue() != null && admRolesUsersList.getSelectedValue() != null) {
                User user = (User) admRolesUsersList.getSelectedValue();
                Role role = (Role) admRolesRolesList.getSelectedValue();
                user.setCanAssign(role, canAssignCheckBox.isSelected());
                saveUsersAndRoles();
            }
        });
        canRevokeCheckBox.addChangeListener(e -> {
            if (admRolesRolesList.getSelectedValue() != null && admRolesUsersList.getSelectedValue() != null) {
                User user = (User) admRolesUsersList.getSelectedValue();
                Role role = (Role) admRolesRolesList.getSelectedValue();
                user.setCanRevoke(role, canRevokeCheckBox.isSelected());
                saveUsersAndRoles();
            }
        });

        refreshDirsList();
    }

    public static void main(String[] args) {
        SecurityLevelSetting securityLevelSetting = new SecurityLevelSetting();
    }

    @SuppressWarnings("unchecked")
    private void refreshDirsList() {
        DefaultListModel listModel = new DefaultListModel<>();
        if (filesHandler.getCurrentDirPath().getParent() != null) listModel.addElement("...");
        for (Path entry : filesHandler.getDirContent()) {
            listModel.addElement(entry.getFileName());
        }
        dirsList.setModel(listModel);
    }

    @SuppressWarnings("unchecked")
    private void refreshUsersList() {
        DefaultListModel listModel = new DefaultListModel<>();
        DefaultComboBoxModel<User> comboBoxModel = new DefaultComboBoxModel<>();
        for (User user : users) {
            listModel.addElement(user);
            comboBoxModel.addElement(user);
        }
        usersList.setModel(listModel);
        admRolesUsersList.setModel(listModel);
        currentUserComboBox.setModel(comboBoxModel);
    }

    @SuppressWarnings("unchecked")
    private void refreshRolesLists() {
        DefaultListModel listModel = new DefaultListModel<>();
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        for (Role role : roles) {
            listModel.addElement(role);
            comboBoxModel.addElement(role);
        }
        rolesList.setModel(listModel);
        admRolesRolesList.setModel(listModel);
        chooseRoleComboBox.setModel(comboBoxModel);
    }

    private void saveUsersAndRoles() {
        try (FileOutputStream fos = new FileOutputStream("users.dat");
        ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(users);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fos = new FileOutputStream("roles.dat");
             ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(roles);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void refreshUsersRolesList() {
        DefaultListModel listModel = new DefaultListModel<>();
        User user = (User) usersList.getSelectedValue();
        if (user == null) return;
        user.getRoles().forEach(listModel::addElement);
        userRolesList.setModel(listModel);
    }
}
