import Controllers.FilesHandler;
import FileSystemWork.DirectoryReader;
import Security.SecurityLevels;
import models.User;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Roman 02.04.2016.
 */
public class FilesHandLing extends JFrame {
    private JPanel rootPanel;
    private JList list1;
    private JList list2;
    private JButton buttonCopy;
    private JLabel dirSLLabel;
    private JLabel fileSLLabel;
    private JList logList;
    private JComboBox chooseUserComboBox;
    private FilesHandler filesHandler = new FilesHandler();
    private Set<User> users;
    private User currentUser;

    @SuppressWarnings("unchecked")
    FilesHandLing() {
        filesHandler.setCurrentDirPath(System.getenv("ROOT_DIR"));
        filesHandler.setCurrentFilePath(System.getenv("ROOT_DIR"));
        filesHandler.setDirContent(DirectoryReader.getDirsList(filesHandler.getCurrentDirPath()));
        filesHandler.setFilesDirContent(DirectoryReader.getDirAndFilesList(filesHandler.getCurrentFilePath()));


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(rootPanel);
        pack();
        loadAndRefreshUsersComboBox();
        setVisible(true);
        buttonCopy.addActionListener(e -> {
            int listSelectedIndex = (filesHandler.getCurrentFilePath().getParent() != null) ? list2.getSelectedIndex() - 1 : list2.getSelectedIndex();
            ListModel listModel =  logList.getModel();
            DefaultListModel newListModel = new DefaultListModel();
            for (int i = 0; i < listModel.getSize(); i++) {
                newListModel.addElement(listModel.getElementAt(i));
            }
            if (filesHandler.copyFile(listSelectedIndex)) {
                newListModel.addElement(new Date() + " Copied succesful!");
            } else {
                newListModel.addElement(new Date() + " Copying failed,");
                newListModel.addElement("destination dir has less security level!");
            }
            logList.setModel(newListModel);
            dirListRefresh();
        });

        list1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    int index = list1.getSelectedIndex();
                    if (filesHandler.dirHasParent()) {
                        if (index == 0) {
                            filesHandler.goToParentDir();
                        } else {
                            filesHandler.goToLevelDir(index - 1);
                        }
                    } else {
                        filesHandler.goToLevelDir(index);
                    }
                    dirListRefresh();
                    dirSLLabel.setText(String.valueOf("SL = " + SecurityLevels.int2String(filesHandler.getCurrentDirSecurityLevel())));
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        list2.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    int index = list2.getSelectedIndex();
                    if (filesHandler.getCurrentFilePath().getParent() != null) {
                        if (index == 0) {
                            filesHandler.goToFilesParentDir();
                        } else {
                            filesHandler.goToLevelFileDir(index - 1);
                        }
                    } else {
                        filesHandler.goToLevelFileDir(index);
                    }
                    fileDirListRefresh();
                    fileSLLabel.setText(String.valueOf("SL = " + SecurityLevels.int2String(filesHandler.getCurrentFileSecurityLevel())));
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        list1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 2) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    //System.out.println(index);
                    if (filesHandler.dirHasParent()) {
                        if (index == 0) {
                            filesHandler.goToParentDir();
                        } else {
                            filesHandler.goToLevelDir(index - 1);
                        }
                    } else {
                           filesHandler.goToLevelDir(index);
                    }
                    dirListRefresh();
                    dirSLLabel.setText(String.valueOf("SL = " + SecurityLevels.int2String(filesHandler.getCurrentDirSecurityLevel())));
                }
            }
        });
        list2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                JList theList = (JList) mouseEvent.getSource();
                if (mouseEvent.getClickCount() == 2) {
                    int index = theList.locationToIndex(mouseEvent.getPoint());
                    if (filesHandler.fileDirHasParent()) {
                        if (index == 0) {
                            filesHandler.goToFilesParentDir();
                        } else {
                            filesHandler.goToLevelFileDir(index - 1);
                        }
                    } else {
                        filesHandler.goToLevelFileDir(index);
                    }
                    fileDirListRefresh();
                    fileSLLabel.setText(String.valueOf("SL = " + SecurityLevels.int2String(filesHandler.getCurrentFileSecurityLevel())));
                }
            }
        });

        chooseUserComboBox.addItemListener(e -> {
            System.out.println("listen");
            currentUser = (User) chooseUserComboBox.getSelectedItem();
            if (currentUser != null) {
                filesHandler.setCurrentUserLevel(currentUser.getSecurityLevel());
            }
            dirListRefresh();
            fileDirListRefresh();
        });

        currentUser = (User) chooseUserComboBox.getSelectedItem();
        if (currentUser != null) {
            filesHandler.setCurrentUserLevel(currentUser.getSecurityLevel());
        }
        filesHandler.setLevelDirContent(DirectoryReader.getDirsList(filesHandler.getCurrentDirPath(), currentUser.getSecurityLevel()));
        filesHandler.setLevelFilesDirContent(DirectoryReader.getDirAndFilesList(filesHandler.getCurrentFilePath(), currentUser.getSecurityLevel()));
        dirListRefresh();
        fileDirListRefresh();
    }

    public static void main(String[] args) {
        FilesHandLing filesHandLing = new FilesHandLing();
    }

    @SuppressWarnings("unchecked")
    private void dirListRefresh() {
        List<Path> list = filesHandler.getLevelDirContent();
        DefaultListModel listModel = new DefaultListModel();
        if (filesHandler.dirHasParent()) listModel.addElement("...");
        for (Path path : list) {
            listModel.addElement(path.getFileName().toString() + " [" + SecurityLevels.int2String(DirectoryReader.getSecurityLevel(path)) + "]");
        }
        list1.setModel(listModel);
    }

    @SuppressWarnings("unchecked")
    private void fileDirListRefresh() {
        List<Path> list = filesHandler.getLevelFilesDirContent();
        DefaultListModel listModel = new DefaultListModel();
        if (filesHandler.fileDirHasParent()) listModel.addElement("...");
        for (Path path : list) {
            String newElement = (Files.isDirectory(path)) ? path.getFileName().toString() + " [" +
                    SecurityLevels.int2String(DirectoryReader.getSecurityLevel(path)) + "]" :
                    path.getFileName().toString();
            listModel.addElement(newElement);
        }
        list2.setModel(listModel);
    }

    @SuppressWarnings("unchecked")
    private void loadAndRefreshUsersComboBox() {

        if (Files.exists(Paths.get("users.dat"))) {
            try (FileInputStream fis = new FileInputStream("users.dat");
                 ObjectInputStream oin = new ObjectInputStream(fis)) {
                users = (Set<User>) oin.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            users = new HashSet<>();
        }
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel();
        for (User user : users) {
            comboBoxModel.addElement(user);
        }
        chooseUserComboBox.setModel(comboBoxModel);
    }
}
