package Controllers;

import FileSystemWork.DirectoryReader;
import Security.SecurityLevels;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

/**
 * Roman 02.04.2016.
 */
public class FilesHandler {

    private Path root = Paths.get(System.getenv("ROOT_DIR"));
    private Path currentDirPath;
    private int currentDirSecurityLevel = 0;
    private Path currentFilePath;
    private int currentFileSecurityLevel;
    private List<Path> dirContent;
    private List<Path> filesDirContent;
    private List<Path> levelDirContent;
    private List<Path> levelFilesDirContent;
    private int currentUserLevel;

    public boolean copyFile(int index) {
        if (currentDirSecurityLevel >= currentFileSecurityLevel) {
            try {
                Path file = filesDirContent.get(index);
                Files.copy(file, currentDirPath.resolve(file.getFileName()));
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //throw new SecurityException("Directory with file has bigger security level than target directory!");
        return false;
    }

    public void goToDir(int index) {
        currentDirPath = dirContent.get(index);
        int newSecurityLevel = DirectoryReader.getSecurityLevel(currentDirPath);
        if (newSecurityLevel > currentDirSecurityLevel) {
            currentDirSecurityLevel = newSecurityLevel;
        }
        refreshDirContent();
    }

    public void goToLevelDir(int index) {
        currentDirPath = levelDirContent.get(index);
        int newSecurityLevel = DirectoryReader.getSecurityLevel(currentDirPath);
        if (newSecurityLevel > currentDirSecurityLevel) {
            currentDirSecurityLevel = newSecurityLevel;
        }
        refreshLevelDirContent();
    }

    public void goToFileDir(int index) {
        Path newCurrentFilePath = filesDirContent.get(index);
        if (Files.isDirectory(newCurrentFilePath)) {
            currentFilePath = newCurrentFilePath;
        }
        int newSecurityLevel = DirectoryReader.getSecurityLevel(currentFilePath);
        if (newSecurityLevel > currentFileSecurityLevel) {
            currentFileSecurityLevel = newSecurityLevel;
        }
        refreshFilesDirContent();
    }

    public void goToLevelFileDir(int index) {
        Path newCurrentFilePath = levelFilesDirContent.get(index);
        if (Files.isDirectory(newCurrentFilePath)) {
            currentFilePath = newCurrentFilePath;
        }
        int newSecurityLevel = DirectoryReader.getSecurityLevel(currentFilePath);
        if (newSecurityLevel > currentFileSecurityLevel) {
            currentFileSecurityLevel = newSecurityLevel;
        }
        refreshLevelFilesDirContent();
    }

    public void goToFilesParentDir() {
        currentFileSecurityLevel = DirectoryReader.getParentSecurityLevel(currentFilePath);
        currentFilePath = currentFilePath.getParent();
        refreshFilesDirContent();
        refreshLevelFilesDirContent();
    }

    public void goToParentDir() {
        currentDirSecurityLevel = DirectoryReader.getParentSecurityLevel(currentDirPath);
        currentDirPath = currentDirPath.getParent();
        refreshDirContent();
        refreshLevelDirContent();
    }

    public int getCurrentDirSecurityLevel() {
        return currentDirSecurityLevel;
    }

    public int getCurrentFileSecurityLevel() {
        return currentFileSecurityLevel;
    }

    public List<Path> getDirContent() {
        return dirContent;
    }

    public List<Path> getFilesDirContent() {
        return filesDirContent;
    }

    public void setDirContent(List<Path> dirContent) {
        this.dirContent = dirContent;
    }

    public void setFilesDirContent(List<Path> filesDirContent) {
        this.filesDirContent = filesDirContent;
    }

    public Path getDirPath(int index) {
        return dirContent.get(index);
    }

    public Path getFileDirPath(int index) {
        return filesDirContent.get(index);
    }

    public Path getCurrentDirPath() {
        return currentDirPath;
    }

    public void setCurrentDirPath(Path currentDirPath) {
        this.currentDirPath = currentDirPath;
    }

    public void setCurrentDirPath(String currentDirPath) {
        this.currentDirPath = Paths.get(currentDirPath);
    }

    public Path getCurrentFilePath() {
        return currentFilePath;
    }

    public void setCurrentFilePath(String currentFilePath) {
        this.currentFilePath = Paths.get(currentFilePath);
    }

    public void setCurrentFilePath(Path currentFilePath) {
        this.currentFilePath = currentFilePath;
    }

    private void refreshDirContent() {
        dirContent = DirectoryReader.getDirsList(currentDirPath);
    }

    private void refreshLevelDirContent() {
        levelDirContent = DirectoryReader.getDirsList(currentDirPath, currentUserLevel);
    }

    private void refreshFilesDirContent() {
        List<Path> filesAndDirs = DirectoryReader.getDirAndFilesList(currentFilePath);
        filesAndDirs.remove(currentFilePath.resolve(Paths.get("settings.dat")));
        filesDirContent = filesAndDirs;
    }

    public List<Path> getLevelDirContent() {
        return levelDirContent;
    }

    public void setLevelDirContent(List<Path> levelDirContent) {
        this.levelDirContent = levelDirContent;
    }

    public List<Path> getLevelFilesDirContent() {
        return levelFilesDirContent;
    }

    public void setLevelFilesDirContent(List<Path> levelFilesDirContent) {
        this.levelFilesDirContent = levelFilesDirContent;
    }

    private void refreshLevelFilesDirContent() {
        List<Path> filesAndDirs = DirectoryReader.getDirAndFilesList(currentFilePath, currentUserLevel);
        filesAndDirs.remove(currentFilePath.resolve(Paths.get("settings.dat")));
        levelFilesDirContent = filesAndDirs;
    }

    public boolean dirHasParent() {
        return !(currentDirPath.equals(root) || currentDirPath.getParent() == null);
    }

    public boolean fileDirHasParent() {
        return !(currentFilePath.equals(root) || currentFilePath.getParent() == null);
    }

    public int getCurrentUserLevel() {
        return currentUserLevel;
    }

    public void setCurrentUserLevel(int currentUserLevel) {
        this.currentUserLevel = currentUserLevel;
        refreshLevelDirContent();
        refreshLevelFilesDirContent();
    }
}
