package Security;

/**
 * Roman 07.04.2016.
 */
public class SecurityLevels {
    public static int NON_SECRET = 0;
    public static int SECRET = 1;
    public static int TOP_SECRET = 2;

    public static String int2String(int level) {
        switch (level) {
            case 0: return  "NON SECRET";
            case 1: return "SECRET";
            case 2: return "TOP SECRET";
            default: return "ERROR";
        }
    }

    public static int string2Int(String level) {
        switch (level) {
            case "NON SECRET":
                System.out.println("non secret"); return 0;
            case "SECRET": return 1;
            case "TOP SECRET": return 2;
            default: return 0;
        }
    }
}
