package FileSystemWork;
/**
 * @author Edward Beckett :: <Edward@EdwardBeckett.com>
 * @since :: 7/21/2015
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class WindowsSymlinkUtility {

    private static final String D_LINK = "/D";
    private static final String H_LINK = "/H";
    private static final String J_LINK = "/J";
    private static final String REM_LINK = "rmdir";
    private String command = "";
    private String link = "";
    private String target = "";

    private List<String> commands = Arrays.asList(D_LINK, H_LINK, J_LINK, REM_LINK);

    private void createSymlink(String command, String link, String target) {
        this.command = command;
        this.link = link;
        this.target = target;

        if (!commands.contains(command)) {
            System.out.println(command + " Is not a valid command \n ");
            return;
        }
        runner();
    }


    private void runner() {

        try {

//            String[] values = {"CMD", "/C", "mklink", this.command, this.link, this.target};
            String[] values = {"cmd","/c","mklink", "/d", "c:\\dir1\\dir2", "c:\\dir2"};
            ProcessBuilder builder = new ProcessBuilder(values);
            builder.directory(new File(this.link));
            Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            System.out.printf("Output of running %s is:\n",
                    Arrays.toString(values));
            while ((line = br.readLine()) != null) {
                System.out.println(new String(line.getBytes(), Charset.forName("koi8-r")));
                int exitValue = process.waitFor();
                System.out.println("\n\nExit Value is " + exitValue);
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        (new WindowsSymlinkUtility()).createSymlink(D_LINK, "C:\\dir2", "C:\\dir1\\dir3");
    }
}