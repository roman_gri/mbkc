package FileSystemWork;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Roman 13.04.2016.
 */
public class WindowsSymlinksCreator {

    public static void createLink(Path link, Path source) {
        try {
            String[] values = {"cmd","/c","mklink", "/d", link.toString(), source.toString()};
            ProcessBuilder builder = new ProcessBuilder(values);
            File parentDir = (link.getParent() != null) ? (link.getParent().toFile()) : (Paths.get("/").toFile());

            builder.directory(parentDir);
            Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                int exitValue = process.waitFor();
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void createLink(String link, String source) {
        createLink(Paths.get(link), Paths.get(source));
    }
}
