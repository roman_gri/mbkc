package FileSystemWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/**
 * Roman 02.04.2016.
 */
public class DirectoryReader {

    public static List<Path> getDirsList(Path path) {
        List<Path> result = new ArrayList<>();
        try (DirectoryStream<Path> steam = Files.newDirectoryStream(path)) {
            for (Path entry : steam) {
                if (Files.isDirectory(entry)) {
                    result.add(entry);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Map<String, Path> getDirsMap(Path path) {
        Map<String, Path> result = new HashMap<>();
        try (DirectoryStream<Path> steam = Files.newDirectoryStream(path)) {
            for (Path entry : steam) {
                if (Files.isDirectory(entry)) {
                    result.put(entry.toString(), entry);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Path> getDirAndFilesList(Path path) {
        List<Path> result = new ArrayList<>();
        try (DirectoryStream<Path> steam = Files.newDirectoryStream(path)) {
            for (Path entry : steam) {
                    result.add(entry);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getSecurityLevel(Path dir) {
        if (!Files.isDirectory(dir)) throw new RuntimeException("Path must be a directory");
        Path settingsFile = dir.resolve("settings.dat");
        Optional<Integer> result = Optional.empty();
        if (!Files.exists(settingsFile)) return 0;
        try (BufferedReader reader = Files.newBufferedReader(settingsFile)) {
            Properties properties = new Properties();
            properties.load(reader);
            result = Optional.of(Integer.parseInt(properties.getProperty("securityLevel")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (result.isPresent()) {
            return result.get();
        } else {
            return 0;
        }
    }

    public static int getParentSecurityLevel(Path dir) {
        if (!Files.isDirectory(dir)) throw new RuntimeException("Path must be a directory");
        Optional<Integer> securityLevel = Optional.empty();
        while ((dir = dir.getParent()) != null) {
            Path settingsFile = dir.resolve("settings.dat");
            if (Files.exists(settingsFile)) {
                securityLevel = Optional.of(getSecurityLevel(dir));
            }
        }
        if (securityLevel.isPresent()) {
            return securityLevel.get();
        } else {
            return 0;
        }
    }

    public static void setSecurityLevel(Path dir, int level) {
        if (!Files.isDirectory(dir)) throw new RuntimeException("Path must be a directory");
        if (DirectoryReader.getParentSecurityLevel(dir) > level) return;
        Path settingsFile = dir.resolve("settings.dat");
        Properties settings = new Properties();
        if (!Files.exists(settingsFile)) {
            //создаем файл
            settings.setProperty("securityLevel", String.valueOf(level));
            try (BufferedWriter writer = Files.newBufferedWriter(settingsFile)) {
                settings.store(writer, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //меняем файл
            try (BufferedReader reader = Files.newBufferedReader(settingsFile);
                 BufferedWriter writer = Files.newBufferedWriter(settingsFile)) {
                settings.load(reader);
                settings.setProperty("securityLevel", String.valueOf(level));
                settings.store(writer, "");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setSecurityLevelRecursively(Path dir, int level) {
        try {
            Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    Path settingsFile = dir.resolve("settings.dat");
                    if (Files.exists(settingsFile)) {
                        int currentDirectorySL = DirectoryReader.getSecurityLevel(dir);
                        if (currentDirectorySL <= level) {
                            Files.delete(settingsFile);
                        }
                    }
                    return super.preVisitDirectory(dir, attrs);
                }
            });
            DirectoryReader.setSecurityLevel(dir, level);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Path> getDirsList(Path path, int securityLevel) {
        List<Path> result = new ArrayList<>();
        try (DirectoryStream<Path> steam = Files.newDirectoryStream(path)) {
            for (Path entry : steam) {
                if (Files.isDirectory(entry) && getSecurityLevel(entry) <= securityLevel) {
                    result.add(entry);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Path> getDirAndFilesList(Path path, int securityLevel) {
        List<Path> result = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
            for (Path entry : stream) {
                if (Files.isDirectory(entry)) {
                    if (getSecurityLevel(entry) <= securityLevel) {
                        result.add(entry);
                    }
                } else {
                    result.add(entry);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
