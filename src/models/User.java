package models;

import Security.SecurityLevels;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Roman 27.04.2016.
 */
public class User implements Serializable {
    private final Set<Role> roles;
    private final Set<Role> canAssign;
    private final Set<Role> canRevoke;
    private final String name;

    public User(Set<Role> roles, Set<Role> canAssign, Set<Role> canRevoke, String name) {
        this.roles = roles;
        this.canAssign = canAssign;
        this.canRevoke = canRevoke;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public User(String name) {
        roles = new HashSet<>();
        canAssign = new HashSet<>();
        canRevoke = new HashSet<>();
        this.name = name;
    }

    public boolean canAssign(Role role) {
        return canAssign.contains(role);
    }

    public boolean canRevoke(Role role) {
        return canRevoke.contains(role);
    }

    public void setCanAssign(Role role, boolean value) {
        if (value) {
            canAssign.add(role);
        } else {
            canAssign.remove(role);
        }
    }

    public void setCanRevoke(Role role, boolean value) {
        if (value) {
            canRevoke.add(role);
        } else {
            canRevoke.remove(role);
        }
    }

    public Set<Role> getRoles() {
        return new HashSet<>(roles);
    }

    public Iterator<Role> getRolesIterator() {
        return roles.iterator();
    }

    public boolean addRole(Role role) {
        return roles.add(role);
    }

    public boolean deleteRole(Role role) {
        return roles.remove(role);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return name.equals(user.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public int getSecurityLevel() {
        boolean isSecret = false;
        for (Role role : roles) {
            if (role.isTopSecret()) {
                return SecurityLevels.TOP_SECRET;
            } else {
                if (role.isSecret()) {
                    isSecret = true;
                }
            }
        }
        if (isSecret) return SecurityLevels.SECRET;
        else return SecurityLevels.NON_SECRET;
    }
}
