package models;

import java.io.Serializable;

/**
 * Roman 27.04.2016.
 */
public class Role implements Serializable {
    private final String name;
    private boolean secret;
    private boolean topSecret;

    public boolean isSecret() {
        return secret;
    }

    public void setSecret(boolean secret) {
        this.secret = secret;
    }

    public boolean isTopSecret() {
        return topSecret;
    }

    public void setTopSecret(boolean topSecret) {
        this.topSecret = topSecret;
    }

    public Role(String name) {
        this.name = name;
    }

    public Role(String name, boolean secret, boolean topSecret) {
        this.name = name;
        this.secret = secret;
        this.topSecret = topSecret;
        if (topSecret) secret = true;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name + "[" + ((secret)?"+, ":"-, ") + ((topSecret)?"+]":"-]");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return name.equals(role.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
