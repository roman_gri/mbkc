package FileSystemWork;

import org.junit.Test;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Roman 13.04.2016.
 */
public class WindowsSymlinksCreatorTest {
    @Test
    public void createLink() throws Exception {
        WindowsSymlinksCreator.createLink("C:\\dir1\\dir2", "C:\\dir2");
        assertTrue(DirectoryReader.getDirAndFilesList(Paths.get("C:\\dir1")).contains(Paths.get("C:\\dir1\\dir2")));
        Files.delete(Paths.get("C:\\dir1\\dir2"));
    }

}